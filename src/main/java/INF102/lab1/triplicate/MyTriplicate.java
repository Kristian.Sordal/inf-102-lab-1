package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyTriplicate<T> implements ITriplicate<T> {
    @Override
    public T findTriplicate(List<T> list) {
        Set<T> set = new HashSet<T>(list.size());
        set.addAll(list);

        for (T item : set) {
            if (Collections.frequency(list, item) >= 3) {
                System.out.println(item);
                return item;
            }
        }
        return null;
    }
}

        // int n = list.size();

        // for (int i = 0; i < n; i++) {
        // T num = list.get(i);
        // int occurences = 0;
        // for (int j = 0; j < n; j++) {
        // if (num.equals(list.get(j))) {
        // occurences++;
        // }
        // }
        // if (occurences >= 3) {
        // System.out.println(num);
        // return num;
        // }
        // }
        // return null;

        // int listSize = list.size();